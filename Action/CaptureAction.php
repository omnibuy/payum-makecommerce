<?php

namespace Omnibuy\MakeCommerce\Action;

use Exception;
use Maksekeskus\Maksekeskus;
use Payum\Core\Action\ActionInterface;
use Payum\Core\Bridge\Spl\ArrayObject;
use Payum\Core\GatewayAwareTrait;
use Payum\Core\Reply\HttpRedirect;
use Payum\Core\Reply\HttpResponse;
use Payum\Core\Request\Capture;
use Payum\Core\Exception\RequestNotSupportedException;
use Payum\Core\Request\GetHttpRequest;
use Payum\Core\Request\RenderTemplate;
use Payum\Core\GatewayAwareInterface;
use Payum\Core\Security\GenericTokenFactoryAwareInterface;
use Payum\Core\Security\GenericTokenFactoryAwareTrait;

class CaptureAction implements ActionInterface, GatewayAwareInterface, GenericTokenFactoryAwareInterface
{
    use GatewayAwareTrait;
    use GenericTokenFactoryAwareTrait;

    private $isSandbox;
    private $templateName;
    private $key;
    private $secret;
    private $shopId;
    private $country;
    private $defaultMethod;

    public function __construct(
        $isSandbox,
        $templateName,
        $key,
        $secret,
        $shopId,
        $country = null,
        $defaultMethod = null
    ) {
        $this->isSandbox = $isSandbox;
        $this->templateName = $templateName;
        $this->key = $key;
        $this->secret = $secret;
        $this->shopId = $shopId;
        $this->country = $country;
        $this->defaultMethod = $defaultMethod;
    }

    /**
     * {@inheritDoc}
     *
     * @param Capture $request
     */
    public function execute($request)
    {
        RequestNotSupportedException::assertSupports($this, $request);

        $model = ArrayObject::ensureArrayObject($request->getModel());
        $token = $request->getToken();

        $makeCommerce = new Maksekeskus($this->shopId, $this->key, $this->secret, $this->isSandbox);

        $getHttpRequest = new GetHttpRequest();
        $this->gateway->execute($getHttpRequest);

        if ($this->isTokenResponse($getHttpRequest)) {
            $paymentToken = $this->parsePaymentToken($getHttpRequest);
            $this->captureTokenPayment($makeCommerce, $paymentToken, $model);
            return;
        } else if ($this->isSignatureReponse($getHttpRequest)) {
            $this->captureSignaturePayment($makeCommerce, $getHttpRequest, $model);
            return;
        }

        $notifyToken = $this->tokenFactory->createNotifyToken(
            $token->getGatewayName(),
            $token->getDetails()
        );

        $transactionRequest = [
            'transaction' => [
                'amount' => number_format($model['amount'], 2, ".", ""),
                'currency' => 'EUR',
                'reference' => $model['order_reference'],
                'merchant_data' => 1,
                'transaction_url' => [
                    'return_url' => [
                        'url' => $token->getTargetUrl(),
                        'method' => 'POST'
                    ],
                    'cancel_url' => [
                        'url' => $token->getTargetUrl(),
                        'method' => 'POST'
                    ],
                    'notification_url' => [
                        'url' => $notifyToken->getTargetUrl(),
                        'method' => 'POST'
                    ]
                ]
            ],
            'customer' => [
                'email' => $model['client_email'],
                'ip' => $getHttpRequest->clientIp,
                'country' => $this->country ?: 'ee',
                'locale' => 'en'
            ]
        ];

        $transaction = $makeCommerce->createTransaction($transactionRequest);
        $model['transaction_id'] = $transaction->id;

        $paymentLink = null;
        if ($this->defaultMethod) {
            foreach ($transaction->payment_methods->banklinks as $paymentMethod) {
                if ($paymentMethod->name !== $this->defaultMethod) {
                    continue;
                }

                $paymentLink = $paymentMethod->url;
            }
        }

        if ($paymentLink) {
            throw new HttpRedirect($paymentLink);
        }

        // always fall back to credit card
        $this->gateway->execute($renderTemplate = new RenderTemplate($this->templateName, [
            'key' => $this->key,
            'transaction' => $model['transaction_id'],
            'amount' => $model['amount'],
            'currency' => $model['currency_code'],
            'email' => $model['client_email'],
            'description' => $model['description'],
            'url' => $token->getTargetUrl(),
            'sandbox' => $this->isSandbox
        ]));

        throw new HttpResponse($renderTemplate->getResult());
    }

    /**
     * {@inheritDoc}
     */
    public function supports($request)
    {
        return
            $request instanceof Capture &&
            $request->getModel() instanceof \ArrayAccess;
    }

    private function isSignatureReponse(GetHttpRequest $request)
    {
        if ($request->method == 'POST' && isset($request->request['json'])) {
            $json = json_decode(stripslashes($request->request['json']), true);

            if (isset($json['signature'])) {
                return true;
            }
        }

        return false;
    }

    private function isTokenResponse(GetHttpRequest $request)
    {
        if ($request->method == 'POST' && isset($request->request['payment_token'])) {
            return true;
        } elseif ($request->method == 'POST' && isset($request->request['json'])) {
            $json = json_decode(stripslashes($request->request['json']), true);

            if (isset($json['token']) && is_array($json['token'])) {
                return true;
            }
        }

        return false;
    }

    private function captureSignaturePayment(Maksekeskus $makeCommerce, GetHttpRequest $request, &$model)
    {
        if (!$makeCommerce->verifySignature($request->request)) {
            $model['status'] = 'error';
            $model['error'] = 'Failed to verify signature';

            return;
        }

        $payment = $makeCommerce->extractRequestData($request->request);
        $model['status'] = $payment['status'];
    }

    private function parsePaymentToken(GetHttpRequest $request)
    {
        if ($request->method == 'POST' && isset($request->request['payment_token'])) {
            return $request->request['payment_token'];
        } elseif ($request->method == 'POST' && isset($request->request['json'])) {
            $json = json_decode(stripslashes($request->request['json']), true);

            if (isset($json['token']) && isset($json['token']['id'])) {
                return $json['token']['id'];
            }
        }

        return null;
    }

    private function captureTokenPayment(Maksekeskus $makeCommerce, $paymentToken, &$model)
    {
        $requestBody = [
            'amount' => $model['amount'],
            'currency' => $model['currency_code'],
            'token' => $paymentToken
        ];

        try {
            $payment = $makeCommerce->createPayment($model['transaction_id'], $requestBody);
        } catch (Exception $exception) {
            $model['status'] = 'error';
            $model['error'] = $exception->getMessage();

            return;
        }

        if ($payment->status && $payment->transaction->status) {
            $model['status'] = $payment->transaction->status;
        }
    }
}
