<?php

namespace Omnibuy\MakeCommerce\Action;

use Maksekeskus\Maksekeskus;
use Payum\Core\Action\ActionInterface;
use Payum\Core\Bridge\Spl\ArrayObject;
use Payum\Core\GatewayAwareTrait;
use Payum\Core\Request\Capture;
use Payum\Core\Exception\RequestNotSupportedException;
use Payum\Core\Request\GetHttpRequest;
use Payum\Core\Request\Notify;
use Payum\Core\GatewayAwareInterface;
use Payum\Core\Security\GenericTokenFactoryAwareTrait;

class NotifyAction implements ActionInterface, GatewayAwareInterface
{
    use GatewayAwareTrait;
    use GenericTokenFactoryAwareTrait;

    private $isSandbox;
    private $key;
    private $secret;
    private $shopId;

    public function __construct(
        $isSandbox,
        $key,
        $secret,
        $shopId
    ) {
        $this->isSandbox = $isSandbox;
        $this->key = $key;
        $this->secret = $secret;
        $this->shopId = $shopId;
    }

    /**
     * {@inheritDoc}
     *
     * @param Capture $request
     */
    public function execute($request)
    {
        RequestNotSupportedException::assertSupports($this, $request);

        $model = ArrayObject::ensureArrayObject($request->getModel());

        $makeCommerce = new Maksekeskus($this->shopId, $this->key, $this->secret, $this->isSandbox);

        $getHttpRequest = new GetHttpRequest();
        $this->gateway->execute($getHttpRequest);

        if ($this->isPaymentResponse($getHttpRequest)) {
            $this->verifyPayment($makeCommerce, $getHttpRequest, $model);
            return;
        }
    }

    /**
     * {@inheritDoc}
     */
    public function supports($request)
    {
        return
            $request instanceof Notify &&
            $request->getModel() instanceof \ArrayAccess;
    }

    private function isPaymentResponse(GetHttpRequest $request)
    {
        if ($request->method == 'POST' && isset($request->request['json'])) {
            $json = json_decode(stripslashes($request->request['json']), true);

            if (isset($json['status'])) {
                return true;
            }
        }

        return false;
    }

    private function verifyPayment(Maksekeskus $makeCommerce, GetHttpRequest $request, &$model)
    {
        $status = null;
        if ($request->method == 'POST' && isset($request->request['json'])) {
            $json = json_decode(stripslashes($request->request['json']), true);

            if (isset($json['status'])) {
                $status = $json['status'];
            }
        }

        if (!$status) {
            return;
        }

        if (!$makeCommerce->verifyMac($request->request)) {
            return;
        }

        $model['status'] = $status;
    }
}
