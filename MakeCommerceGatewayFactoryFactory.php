<?php

namespace Omnibuy\MakeCommerce;

use Payum\Core\GatewayFactoryInterface;

class MakeCommerceGatewayFactoryFactory
{
    public function getCallable()
    {
        return [$this, 'getGatewayFactory'];
    }

    public function getGatewayFactory(array $config, GatewayFactoryInterface $coreGatewayFactory)
    {
        return new MakeCommerceGatewayFactory($config, $coreGatewayFactory);
    }
}
