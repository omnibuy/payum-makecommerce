<?php

namespace Omnibuy\MakeCommerce;

use Omnibuy\MakeCommerce\Action\ConvertPaymentAction;
use Omnibuy\MakeCommerce\Action\CaptureAction;
use Omnibuy\MakeCommerce\Action\NotifyAction;
use Omnibuy\MakeCommerce\Action\StatusAction;
use Payum\Core\Bridge\Spl\ArrayObject;
use Payum\Core\GatewayFactory;

class MakeCommerceGatewayFactory extends GatewayFactory
{
    /**
     * {@inheritDoc}
     */
    protected function populateConfig(ArrayObject $config)
    {
        $config['payum.default_options'] = [
            'sandbox' => true,
            'payum.template.credit_card_dialog' => '@PayumMakeCommerce/Action/credit_card_dialog.html.twig'
        ];
        $config->defaults($config['payum.default_options']);

        $config->defaults([
            'payum.factory_name' => 'makecommerce',
            'payum.factory_title' => 'MakeCommerce',
            'payum.action.capture' => new CaptureAction(
                $config['sandbox'],
                $config['payum.template.credit_card_dialog'],
                $config['key'],
                $config['secret'],
                $config['shop_id'],
                $config['country'],
                $config['default_method']
            ),
            'payum.action.notify' => new NotifyAction(
                $config['sandbox'],
                $config['key'],
                $config['secret'],
                $config['shop_id']
            ),
            'payum.action.status' => new StatusAction(),
            'payum.action.convert_payment' => new ConvertPaymentAction()
        ]);

        $config['payum.required_options'] = [
            'key',
            'secret',
            'shop_id'
        ];

        $config->validateNotEmpty($config['payum.required_options']);

        $config['payum.paths'] = array_replace([
            'PayumMakeCommerce' => __DIR__.'/Resources/views',
        ], $config['payum.paths'] ?: []);
    }
}
