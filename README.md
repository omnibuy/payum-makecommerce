## Usage with Symfony

Please see the [example project](https://bitbucket.org/omnibuy/payum-makecommerce-example) for usage with Symfony. 
Current integration only includes the Credit Card Dialog as payment method. Other payment methods are coming soon.

## Development environment

Project comes with Docker development environment

- `docker compose up`
- `docker compose exec fpm composer install`

## Running tests

There are currently no tests.

## License

Project is released under the [MIT License](LICENSE).
